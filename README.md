# sjLightMon
sjLightMon is an efficient, lightweight system monitoring tool, inspired by BusyBox. Its minimalist design is perfect for environments with limited resources, ensuring essential vigilance.
